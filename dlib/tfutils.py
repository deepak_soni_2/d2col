import numpy as np
import tensorflow as tf
import dlib.dutils as dutils
import os

np.random.random_integers()

'''
Updates : separate, generator and display of batch_generator_display
'''

class GanTrainer:
    '''
    This class trains a gan. The compiled models can be either set directly by providing gan object or
    by providing path to saved model. The train function handles the training, display trigger, model
    saving, reporting, printing report, learning rate update.
    example in dlib.examples.GanTrainer
    Future : repetative training until particular loss is not reached
    '''

    def __init__(self):
        self.generator = None
        self.discriminator = None
        self.gan = None

    def set_model(self, gan, generator_index, discriminator_index):
        """
        This function sets model with a given model instance.
        :param gan: compiled gan model
        :param generator_index: generator index in gan model
        :param discriminator_index: discriminator index in gan model
        """
        self.gan = gan
        self.generator = self.gan.layers[generator_index]
        self.discriminator = self.gan.layers[discriminator_index]

    def set_model_from_save(self, path, generator_index, discriminator_index, d_opt, d_loss):
        """
        This function sets model with a saved model path. The discriminator needs to be compiled and
        can't be saved separately so the optimizers and loss is saved using picle, and loaded while
        model load.
        :param path: path to saved model
        :param generator_index: generator index in gan model
        :param discriminator_index: discriminator index in gan model
        """
        self.gan = tf.keras.models.load_model(path)
        self.generator = self.gan.layers[generator_index]
        self.discriminator = self.gan.layers[discriminator_index]

        self.discriminator.compile(optimizer=d_opt, loss=d_loss, metrics=['accuracy'])

    def train(self, batch_generator_display, batch_count, epochs=10, save_model=None, report=None,
              save_after_batch=1, report_after_batch=1, display_after_batch=None, discriminator_learning_rate=None,
              gan_learning_rate=None, dloss_limit=None, gloss_limit=None):
        """
        trains the gan model
        :param batch_generator_display: function pointer to the batch generator display function. this
            function takes generator instance and display trigger and returns discriminator and gan's
            training x and y in the form (d_set_in, d_set_out, gan_set_in, gan_set_out). The display
            trigger checked in the function and desired output is given.
        :param batch_count: Total number of batches per epoch.
        :param epochs: Number of epochs to run
        :param save_model: folder location where you want to save the model, None if not saving
        :param report: file location where you want to report the training, None if not reporting
        :param save_after_batch: Number of batches after which saving is triggered
        :param report_after_batch: Number of batches after which reporting is triggered
        :param display_after_batch: Number of batches after which display is triggered
        :param discriminator_learning_rate: new learning rates for the discriminator
        :param gan_learning_rate: new learning rates for the gan
        """
        if discriminator_learning_rate is not None:
            tf.keras.backend.set_value(self.discriminator.optimizer.learning_rate, discriminator_learning_rate)
        if gan_learning_rate is not None:
            tf.keras.backend.set_value(self.gan.optimizer.learning_rate, discriminator_learning_rate)
        if display_after_batch is not None:
            assert display_after_batch <= batch_count
        assert save_after_batch <= batch_count
        assert report_after_batch <= batch_count
        dutils.create_file_safely(report, mode='a')
        if os.path.exists(save_model):
            assert input('output directory already exists ' + save_model + ' overwrite y/n ? : ') == 'y'

        acc = [0.0, 0.0, 0.0, 0.0]
        dloss = [100, 100]
        gloss = [100, 100]

        for i in range(1, epochs + 1):
            for j in range(1, batch_count + 1):

                if display_after_batch is not None:
                    if j % display_after_batch == 0:
                        dsp = True
                    else:
                        dsp = False
                else:
                    dsp = False

                d_set_in, d_set_out, gan_set_in, gan_set_out = batch_generator_display(self.generator, show=dsp)
                
                self.discriminator.trainable = True
                if dloss_limit is not None:
                    dloss[0] = 100
                    while dloss[0] > dloss_limit:
                        dloss = self.discriminator.train_on_batch(x=d_set_in, y=d_set_out)
                        print('\ri = ' + str(i) + "  j = " + str(j) + "  :  {:.9f}  ".format(dloss[0]) + "  {:.9f}  ".format(dloss[1]) + "  {:.9f}  ".format(gloss[0]) + "  {:.9f}  ".format(gloss[1]), end='')
                else:
                    dloss = self.discriminator.train_on_batch(x=d_set_in, y=d_set_out)
                    print('\ri = ' + str(i) + "  j = " + str(j) + "  :  {:.9f}  ".format(dloss[0]) + "  {:.9f}  ".format(dloss[1]) + "  {:.9f}  ".format(gloss[0]) + "  {:.9f}  ".format(gloss[1]), end='')
                
                self.discriminator.trainable = False
                if gloss_limit is not None:
                    gloss[0] = 100
                    while gloss[0] > gloss_limit:
                        gloss = self.gan.train_on_batch(x=gan_set_in, y=gan_set_out)
                        print('\ri = ' + str(i) + "  j = " + str(j) + "  :  {:.9f}  ".format(dloss[0]) + "  {:.9f}  ".format(dloss[1]) + "  {:.9f}  ".format(gloss[0]) + "  {:.9f}  ".format(gloss[1]), end='')
                else:
                    gloss = self.gan.train_on_batch(x=gan_set_in, y=gan_set_out)
                    print('\ri = ' + str(i) + "  j = " + str(j) + "  :  {:.9f}  ".format(dloss[0]) + "  {:.9f}  ".format(dloss[1]) + "  {:.9f}  ".format(gloss[0]) + "  {:.9f}  ".format(gloss[1]), end='')

                acc[0] = acc[0] + dloss[0]
                acc[1] = acc[1] + dloss[1]
                acc[2] = acc[2] + gloss[0]
                acc[3] = acc[3] + gloss[1]

                if save_model is not None and j % save_after_batch == 0:
                    tf.keras.models.save_model(self.gan, save_model)
                    # with open(save_model + '/states.pkl', 'wb') as output:
                    #     pickle.dump(tf.keras.backend.eval(self.discriminator.optimizer), output,
                    #                 pickle.HIGHEST_PROTOCOL)
                    #     pickle.dump(tf.keras.backend.eval(self.discriminator.loss), output, pickle.HIGHEST_PROTOCOL)

                if report is not None and j % report_after_batch == 0:
                    file = open(report, "a")
                    acc = np.array(acc)/display_after_batch
                    file.write('i ' + str(i) + '  j ' + str(j) + "  {:.9f}".format(acc[0]) + "  {:.9f}  ".format(
                        acc[1]) + "  {:.9f}".format(acc[2]) + "  {:.9f}  ".format(acc[3]) + '\n')
                    file.close()
                    acc = [0.0, 0.0, 0.0, 0.0]


class DiscriminatorTrainer:
    '''
    This class trains a neural network model. The compiled model can be either set directly by providing model object or
    by providing path to saved model. The train function handles the training, display trigger, model
    saving, reporting, printing report, learning rate update.
    '''

    def __init__(self):
        self.model = None

    def set_model(self, model):
        """
        This function sets model with a given model instance.
        :param model: compiled model
        """
        self.model = model

    def set_model_from_save(self, path):
        """
        This function sets model with a saved model path.
        :param path: path to saved model
        """
        self.model = tf.keras.models.load_model(path)

    def train(self, batch_generator_display, batch_count, epochs=10, save_model=None, report=None,
              save_after_batch=1, report_after_batch=1, display_after_batch=None, learning_rate=None):
        """
        trains the model
        :param batch_generator_display: function pointer to the batch generator display function. this
            function takes model instance and display trigger and returns inputs and outputs to the model
            in the form (inputs, outputs). The display trigger checked in the function and desired output is given.
        :param batch_count: Total number of batches per epoch.
        :param epochs: Number of epochs to run
        :param save_model: folder location where you want to save the model, None if not saving
        :param report: file location where you want to report the training, None if not reporting
        :param save_after_batch: Number of batches after which saving is triggered
        :param report_after_batch: Number of batches after which reporting is triggered
        :param display_after_batch: Number of batches after which display is triggered
        :param learning_rate: new learning rates for the model
        """
        if learning_rate is not None:
            tf.keras.backend.set_value(self.model.optimizer.learning_rate, learning_rate)
        if display_after_batch is not None:
            assert display_after_batch <= batch_count
        assert save_after_batch <= batch_count
        assert report_after_batch <= batch_count
        dutils.create_file_safely(report, mode='a')
        # dutils.create_dir_safely(save_model)
        if os.path.exists(save_model):
            assert input('output directory already exists ' + save_model + ' overwrite y/n ? : ') == 'y'
        
        acc = [0.0, 0.0]

        for i in range(1, epochs + 1):
            for j in range(1, batch_count + 1):

                if display_after_batch is not None:
                    if j % display_after_batch == 0:
                        dsp = True
                    else:
                        dsp = False
                else:
                    dsp = False

                inputs, outputs = batch_generator_display(self.model, show=dsp)
                dloss = self.model.train_on_batch(x=inputs, y=outputs)

                acc[0] = acc[0] + dloss[0]
                acc[1] = acc[1] + dloss[1]

                print('\ri = ' + str(i) + "  j = " + str(j) + "  :  {:.9f}  ".format(dloss[0]) + "  {:.9f}  ".format(
                    dloss[1]), end='')

                if save_model is not None and j % save_after_batch == 0:
                    tf.keras.models.save_model(self.model, save_model)

                if report is not None and j % report_after_batch == 0:
                    file = open(report, "a")
                    acc = np.array(acc)/display_after_batch
                    file.write('i ' + str(i) + '  j ' + str(j) + "  {:.9f}".format(acc[0]) + "  {:.9f}  ".format(
                        acc[1]) + '\n')
                    file.close()
                    acc = [0.0, 0.0]
