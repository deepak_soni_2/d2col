import colorsys
import datetime
import os
import shutil
import zipfile
import cv2
import numpy as np
import pytz
from tqdm import tqdm
import paramiko
import re
from skimage import color


class Graph:
    def __init__(self, lst):
        # Build a directed graph and a list of all names that have no parent
        self.graph = {name: set() for tup in lst for name in tup}
        self.has_parent = {name: False for tup in lst for name in tup}
        self.parent = {}
        for parent, child in lst:
            self.graph[parent].add(child)
            self.has_parent[child] = True
            self.parent[child] = parent

        # All names that have absolutely no parent:
        self.roots = [name for name, parents in self.has_parent.items() if not parents]

    # traversal of the graph (doesn't care about duplicates and cycles)
    def traverse(self, hierarchy, names, op):
        for name in names:
            hierarchy[name] = self.traverse({}, self.graph[name], op)
            op(self, name, hierarchy)
        return hierarchy


def unzip(path_to_zip_file, directory_to_extract_to):
    with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
        zip_ref.extractall(directory_to_extract_to)


def get_images_from_video(vid_path, outdir, skip=0, operations=None, length=None, outtype='jpg'):
    '''
    :param vid_path: path of the video
    :param skip: pick images after this many frames
    :param length: no. of frames to pick, None for complete video
    :param outdir: output directory in which jpgs are saved
    :return: None
    '''
    create_dir_safely(outdir)

    cap = cv2.VideoCapture(vid_path)
    if length == None:
        length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    cap.set(cv2.CAP_PROP_POS_FRAMES, skip)

    def fn(inp):
        return inp

    if operations is None:
        operations = fn
    print('extracting frames')
    for i in tqdm(range(length)):
        ret, frame = cap.read()
        if not ret: break
        frame = operations(frame)
        if outtype == 'jpg':
            cv2.imwrite(filename=outdir + str(i) + '.jpg', img=frame)
        elif outtype == 'png':
            cv2.imwrite(filename=outdir + str(i) + '.png', img=frame)
        elif outtype == 'npy_uint8':
            np.save(file=outdir + str(i) + '.npy', arr=np.array(frame))
        elif outtype == 'npy_int16':
            frame = np.array(frame, dtype=np.int16)
            np.save(file=outdir + str(i) + '.npy', arr=np.array(frame))


def rgb2hsv(rgb):
    '''
    :param rgb: rgb color image
    :return: hsv color image that is compatible with cv2 imshow
    '''
    h, s, v = colorsys.rgb_to_hsv(rgb[0] / 256, rgb[1] / 256, rgb[2] / 256)
    h = h * 192
    s = s * 255
    v = v * 255
    return np.array([h, s, v], dtype=np.uint8)


def write_file(path, text, open_type):
    file = open(path, open_type)
    file.write(text)
    file.close()


def create_dir_safely(outdir, overwrite='n'):
    if os.path.exists(outdir):
        if overwrite is not 'y':
            assert input('output directory already exists ' + outdir + ' overwrite y/n ? : ') == 'y'
        shutil.rmtree(outdir)
    os.mkdir(outdir)


def create_file_safely(outfile):
    mode = 'w'
    if os.path.exists(outfile):
        mode = input('output file already exists ' + outfile + ' overwrite w, append a ? : ')
    file = open(file=outfile, mode=mode)
    file.close()


def get_current_ist_time():
    now = datetime.datetime.now(pytz.utc)
    tz = pytz.timezone('Asia/Kolkata')
    return now.astimezone(tz)


def divide_equal_chunks(l, n):
    n = max(1, n)
    return list((l[i:i+n] for i in range(0, len(l), n)))


def unzip(zip_path, outdir):
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        zip_ref.extractall(outdir)


class ShellHandler:

    def __init__(self, host, user, psw, keyfile):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(host, username=user, password=psw, key_filename=keyfile, port=22)
        self.sftp = paramiko.SFTPClient.from_transport(self.ssh.get_transport())
        print('connected')

        channel = self.ssh.invoke_shell()
        self.stdin = channel.makefile('wb')
        self.stdout = channel.makefile('r')

    def __del__(self):
        self.ssh.close()

    def execute(self, cmd):
        """

        :param cmd: the command to be executed on the remote computer
        :examples:  execute('ls')
                    execute('finger')
                    execute('cd folder_name')
        """
        cmd = cmd.strip('\n')
        self.stdin.write(cmd + '\n')
        finish = 'end of stdOUT buffer. finished with exit status'
        echo_cmd = 'echo {} $?'.format(finish)
        self.stdin.write(echo_cmd + '\n')
        shin = self.stdin
        self.stdin.flush()

        shout = []
        sherr = []
        exit_status = 0
        for line in self.stdout:
            if str(line).startswith(cmd) or str(line).startswith(echo_cmd):
                # up for now filled with shell junk from stdin
                shout = []
            elif str(line).startswith(finish):
                # our finish command ends with the exit status
                exit_status = int(str(line).rsplit(maxsplit=1)[1])
                if exit_status:
                    # stderr is combined with stdout.
                    # thus, swap sherr with shout in a case of failure.
                    sherr = shout
                    shout = []
                break
            else:
                # get rid of 'coloring and formatting' special characters
                shout.append(re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]').sub('', line).
                             replace('\b', '').replace('\r', ''))

        # first and last lines of shout/sherr contain a prompt
        if shout and echo_cmd in shout[-1]:
            shout.pop()
        if shout and cmd in shout[0]:
            shout.pop(0)
        if sherr and echo_cmd in sherr[-1]:
            sherr.pop()
        if sherr and cmd in sherr[0]:
            sherr.pop(0)

        return shin, shout, sherr


def uint2sfloat(array, max_val):
    return ((np.array(array, dtype=np.float))/(max_val/2))-1.0


def sfloats2uint(array, max_val):
    return np.array((array+1.0)*max_val/2, dtype=np.uint8)


def hex2rgb(hexcol):
    """
    converts a '#FFFFFF' format color string to a tuple of (R, G, B) in decimal
    """
    h = hexcol.strip().lstrip('#')
    return tuple(int(h[i:i+2], 16) for i in (0, 2, 4))


def cvlab2rgb(cvlab):
    """
    :param cvlab: lab in l/2.55, a+127, b+127 uint8 format
    :return: rgb in uint8 format
    """
    return np.array(255*color.lab2rgb([[np.array(cvlab, dtype=np.float) - np.array((0, 127, 127))]]), dtype=np.uint8)[0][0]


def rgb2cvlab(rgb):
    '''
    :param rgb: rgb in uint8 format
    :return: lab in l/2.55, a+127, b+127 uint8 format
    '''
    return np.array((color.rgb2lab([[np.array(rgb)/255.0]]) + np.array((0, 127, 127)))[0][0]).astype(np.uint8)


def merge_lab(rgb, lab, dst_path=None):
    """
    returns an image with ab of lab and illumination of rgb and also saves the image in the dst_path if dst_path is not None.
    :param src_path: path to the rgb image or rgb image vector of dimention (h, w, 3) in RGB color space
    :param lab: a vector of dimention (h, w, 3) in LAB color space or path to an image in RGB color space
    :param dst_path: path where the image is to be saved, or None if no saving is needed.
    :return: the resultant BGR image.
    """
    if type(rgb) == str:
        src = cv2.imread(rgb)
    if type(lab) == str:
        lab = cv2.imread(lab)
        lab = cv2.cvtColor(lab, cv2.COLOR_BGR2LAB)
    src = cv2.cvtColor(src, cv2.COLOR_BGR2LAB)
    lab[:, :, 0] = src[:, :, 0]
    lab = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)
    if dst_path is not None:
        cv2.imwrite(dst_path, lab)
    return lab


def amp_image(image, multiplier, dst_path=None, show=False):
    if type(image) == str:
        image = cv2.imread(image)
    image = image * multiplier
    if show:
        cv2.imshow('amp_image', image)
        cv2.waitKey(0)
    if dst_path is not None:
        cv2.imwrite(dst_path, image)
    return image


def sort_number_strings(lst):
    def fn(inp):
      return int(inp.split('.')[0])
    return sorted(lst, key=fn)
