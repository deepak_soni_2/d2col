import os
import numpy as np
import deepcol2.src.utils as utils
import dlib.tfutils as tfutils

utils.full_dataset = np.array(os.listdir(utils.COLOR_PATH))
model = utils.get_generator()
trainer = tfutils.DiscriminatorTrainer()
trainer.set_model(model)
# trainer.set_model_from_save(SOURCE_PATH+'save1', generator_index=1, discriminator_index=2)
print(trainer.model.summary())
trainer.train(batch_generator_display=utils.batch_generator_display,
              batch_count=int(len(utils.full_dataset) / utils.BATCH_SIZE), epochs=500, save_model='save1',
              save_after_batch=20, report='report1.txt', report_after_batch=4, display_after_batch=4,
              learning_rate=0.0001)
