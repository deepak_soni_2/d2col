import cv2
import dlib.dutils as dutils
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

SOURCE_PATH = "/content/drive/My Drive/Colab Notebooks/gancol/"
PROJECT_PATH = "../assets/"
IMAGE_SIZE = 256
COLOR_PATH = PROJECT_PATH + 'color/'
VIDEO_PATH = PROJECT_PATH + "vid_dir/ek_rishta.mp4"
COLOR_BIT = 256
BATCH_SIZE = 32
GAUS_DIST_COEFF = 20
INPUT_SHAPE = (256, 256, 4)
adam = tf.keras.optimizers.Adam(lr=0.0001)
full_dataset = None


def generate_dataset():
    def operator(frame):
        cropped = frame[52:308, 172:428, :]
        cropped = cv2.cvtColor(cropped, cv2.COLOR_RGB2LAB)
        return cropped
    dutils.get_images_from_video(VIDEO_PATH, COLOR_PATH, operations=operator, outtype='jpg')


def batch_generator_display(model, show):
    batch_seq = np.random.randint(0, len(full_dataset), size=BATCH_SIZE)
    images = full_dataset[batch_seq]
    inputs = []
    outputs = []
    for index, image in enumerate(images):
        imint = int(image.split('.')[0])
        rm = int(np.random.normal(imint, 20, 1))
        ref_idx = np.clip(rm, 1, len(full_dataset)-1)
        imgin = cv2.imread(COLOR_PATH+image)
        imgref = cv2.imread(COLOR_PATH+str(ref_idx)+'.jpg')
        inp = np.ones(shape=(IMAGE_SIZE, IMAGE_SIZE, 4), dtype=np.uint8)
        inp[:, :, 0:3] = imgref
        inp[:, :, 3] = imgin[:, :, 0]
        inputs.append(inp)
        outputs.append(imgin[:, :, 1:3])

    if show:
        greys = dutils.sfloats2uint(inputs[0], COLOR_BIT)
        pred = dutils.sfloats2uint(model.predict(inputs[0]), COLOR_BIT)
        actual = dutils.sfloats2uint(outputs[0], COLOR_BIT)

        ref = greys[:, :, 0:3]

        _pred = np.ones(shape=(IMAGE_SIZE, IMAGE_SIZE, 3), dtype=np.uint8)
        _pred[:, :, 0] = greys[:, :, 3]
        _pred[:, :, 1:3] = pred

        _actual = np.ones(shape=(IMAGE_SIZE, IMAGE_SIZE, 3), dtype=np.uint8)
        _actual[:, :, 0] = greys[:, :, 3]
        _actual[:, :, 1:3] = actual

        axis, plots = plt.subplots(1, 7)
        axis.set_figheight(20)
        axis.set_figwidth(20)
        plots[0].imshow(cv2.cvtColor(ref, cv2.COLOR_LAB2BGR))
        plots[2].imshow(cv2.cvtColor(_actual, cv2.COLOR_LAB2BGR))
        plots[1].imshow(cv2.cvtColor(_pred, cv2.COLOR_LAB2BGR))
        plots[3].imshow(_pred[:, :, 1], cmap='Greys_r', vmin=0, vmax=255)
        plots[4].imshow(_actual[:, :, 1], cmap='Greys_r', vmin=0, vmax=255)
        plots[5].imshow(_pred[:, :, 2], cmap='Greys_r', vmin=0, vmax=255)
        plots[6].imshow(_actual[:, :, 2], cmap='Greys_r', vmin=0, vmax=255)
        plt.show()

    return dutils.uint2sfloat(inputs, COLOR_BIT), dutils.uint2sfloat(outputs, COLOR_BIT)


def get_generator():
    dis_in = tf.keras.layers.Input(shape=(256, 256, 4))
    dis_out = tf.keras.layers.Conv2D(64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_in)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(128, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 128
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2D(128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(256, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 64
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2D(256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(512, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 32
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2D(512, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(512, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 16
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 8
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2DTranspose(1024, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 16
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(1024, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2DTranspose(512, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 32
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(512, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(512, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2DTranspose(256, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 64
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2DTranspose(128, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 128
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)

    dis_out = tf.keras.layers.Conv2DTranspose(64, kernel_size=(3, 3), strides=(2, 2), padding='same', activation='relu')(dis_out)  # 256
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(dis_out)
    dis_out = tf.keras.layers.BatchNormalization()(dis_out)
    dis_out = tf.keras.layers.Conv2D(2, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(dis_out)

    generator = tf.keras.models.Model(inputs=dis_in, outputs=dis_out)
    generator.compile(loss=tf.keras.losses.mean_squared_error, metrics=['accuracy'], optimizer=adam)
    return generator
