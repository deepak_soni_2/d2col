import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import dlib.dutils as dutils
import dlib.tfutils as tfutils

adam = tf.keras.optimizers.Adam(lr=0.0001, beta_1=0.5)
RANDOM_DIM = 10
NUM_CLASSES = 10
EPOCHS = 200
INPUT_SHAPE = NUM_CLASSES + RANDOM_DIM
BATCH_SIZE = 128
MAX_VAL = 256

mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = dutils.uint2sfloat(x_train, MAX_VAL)
x_train = x_train.reshape(60000, 784)
y_train = tf.keras.utils.to_categorical(y_train, 10)


def get_generator():
    gen_in_z = tf.keras.layers.Input(shape=(RANDOM_DIM,))
    gen_in_label = tf.keras.layers.Input(shape=(NUM_CLASSES,))
    gen_out = tf.keras.layers.Concatenate()([gen_in_z, gen_in_label])
    gen_out = tf.keras.layers.Dense(256)(gen_out)
    gen_out = tf.keras.layers.LeakyReLU(0.2)(gen_out)
    gen_out = tf.keras.layers.Dense(512)(gen_out)
    gen_out = tf.keras.layers.LeakyReLU(0.2)(gen_out)
    gen_out = tf.keras.layers.Dense(1024)(gen_out)
    gen_out = tf.keras.layers.LeakyReLU(0.2)(gen_out)
    gen_out = tf.keras.layers.Dense(784, activation='tanh')(gen_out)
    generator = tf.keras.models.Model(inputs=[gen_in_z, gen_in_label], outputs=gen_out)
    return generator


def get_discriminator():
    dis_in_image = tf.keras.layers.Input(shape=(784,))
    dis_in_label = tf.keras.layers.Input(shape=(NUM_CLASSES,))
    dis_out = tf.keras.layers.Concatenate()([dis_in_image, dis_in_label])
    dis_out = tf.keras.layers.Dense(1024)(dis_out)
    dis_out = tf.keras.layers.LeakyReLU(0.2)(dis_out)
    dis_out = tf.keras.layers.Dropout(0.3)(dis_out)
    dis_out = tf.keras.layers.Dense(512)(dis_out)
    dis_out = tf.keras.layers.LeakyReLU(0.2)(dis_out)
    dis_out = tf.keras.layers.Dropout(0.3)(dis_out)
    dis_out = tf.keras.layers.Dense(256)(dis_out)
    dis_out = tf.keras.layers.LeakyReLU(0.2)(dis_out)
    dis_out = tf.keras.layers.Dropout(0.3)(dis_out)
    dis_out = tf.keras.layers.Dense(1, activation='sigmoid')(dis_out)
    discriminator = tf.keras.models.Model(inputs=[dis_in_image, dis_in_label], outputs=dis_out)
    return discriminator


def batch_generator_display(generator, show):
    batch_seq = np.random.randint(0, len(x_train), size=BATCH_SIZE)
    sample_labels = y_train[batch_seq]
    sample_images = x_train[batch_seq]

    noise = np.random.normal(0, 1, size=[BATCH_SIZE, RANDOM_DIM])
    gan_set_in = [noise, sample_labels]
    gan_set_out = np.ones([BATCH_SIZE], dtype=np.float32)

    generated = generator.predict(gan_set_in)
    p = np.random.permutation(2 * BATCH_SIZE)
    images_batch = np.concatenate([sample_images, generated])[p]
    label_batch = np.concatenate([sample_labels, sample_labels])[p]
    d_set_in = [images_batch, label_batch]
    d_set_out = np.concatenate([np.ones([BATCH_SIZE], dtype=np.float32), np.zeros([BATCH_SIZE], dtype=np.float32)])[p]

    if show:
        plt.imshow(np.reshape(dutils.sfloats2uint(generated[0], MAX_VAL), (28, 28)))
        plt.show()

    return d_set_in, d_set_out, gan_set_in, gan_set_out


discriminator = get_discriminator()
generator = get_generator()
discriminator.compile(loss='binary_crossentropy', metrics=['accuracy'], optimizer=adam)
discriminator.trainable = False
gan_in_z = tf.keras.layers.Input(shape=(RANDOM_DIM,))
gan_in_label = tf.keras.layers.Input(shape=(NUM_CLASSES,))
tmp_image = generator([gan_in_z, gan_in_label])
gan_out = discriminator([tmp_image, gan_in_label])
gan = tf.keras.models.Model(inputs=[gan_in_z, gan_in_label], outputs=gan_out)
gan.compile(loss='binary_crossentropy', metrics=['accuracy'], optimizer=adam)
# gan.summary()

Trainer = tfutils.GanTrainer()
# Trainer.set_model(gan, generator_index=2, discriminator_index=3)
Trainer.set_model_from_save('save2', generator_index=2, discriminator_index=3)
Trainer.train(batch_generator_display=batch_generator_display, batch_count=int(len(x_train) / BATCH_SIZE), epochs=5,
              save_model='save2', save_after_batch=100, report='report1.txt', report_after_batch=20,
              display_after_batch=50)
