# def merge(self):
#     self.create_id_maps()
#     dutils.create_dir_safely(self.outdir, overwrite='y')
#     folders = os.listdir(self.insdir)
#     for folder in tqdm(folders):
#         files = os.listdir(os.path.join(self.insdir, folder))
#         # dim = cv2.imread(os.path.join(self.insdir, folder, files[0])).shape
#         # fin = np.zeros((dim[0], dim[1], 3), dtype=np.float)
#         fin = np.zeros((1080, 1920, 3), dtype=np.float)
#         divisor = np.ones_like(fin) * 0.00000001
#         # for file in files:
#         #     img = cv2.imread(os.path.join(self.insdir, folder, file))
#         #     divtmp = img
#         #     cur = np.zeros(fin.shape, dtype=np.uint8)
#         #     divisor = divisor + divtmp
#         #     cur[img[:, :, 0] > 0] = dutils.rgb2cvlab(literal_eval(self.insid2fill[int(file.split('_')[1].split('.')[0])]))
#         #     fin = fin + cur
#
#         info = json.load(open(os.path.join(self.pandir, folder, 'info.json')))
#         pngid2contid = {item["id"]: item["category_id"] for item in info}
#         pngid2fill = {item: self.contid2fill[pngid2contid[item]] for item in pngid2contid}
#         img_ori = cv2.imread(os.path.join(self.pandir, folder, 'ids.png'))
#         for item in pngid2fill:
#             img = copy(img_ori)
#             img[img != item] = 0
#             img[img == item] = 1
#             divtmp = img
#             cur = np.zeros(fin.shape, dtype=np.uint8)
#             divisor = divisor + divtmp
#             cur[img[:, :, 0] == 1] = dutils.rgb2cvlab(literal_eval(pngid2fill[item]))
#             fin = fin + cur
#         fin = np.divide(fin, divisor)
#         # fin = 0.0 * fin
#
#         # for item in range(0, 11):
#         #     val = cv2.imread(os.path.join(self.indir, 'sem_seg/pngs/', folder, str(item) + '.png'))
#         # print(np.amax(val))
#         # mx = np.amax(val)
#         # val = val/35.0
#         # val = np.power(val, 1.5)
#         # val = np.sqrt(val)
#         # val = val*35.0
#         # val[val < np.amax(val)*0.35] = 0
#         # fin[:, :, 0] = fin[:, :, 0] + 0.04 * val[:, :, 0] * literal_eval(self.panid2fill[item])[2]
#         # fin[:, :, 1] = fin[:, :, 1] + 0.04 * val[:, :, 1] * literal_eval(self.panid2fill[item])[1]
#         # fin[:, :, 2] = fin[:, :, 2] + 0.04 * val[:, :, 2] * literal_eval(self.panid2fill[item])[0]
#
#         # yo = np.zeros_like(fin)
#         # for item in range(0, 5):
#         #     print('reading : ', item)
#         #     val = cv2.imread(os.path.join(self.indir, 'sem_seg/pngs/', folder, str(item) + '.png'))
#         #     yo = yo + val
#
#         # cv2.imwrite(os.path.join(self.outdir, folder + '.jpg'), fin.astype(np.uint8))
#         # cv2.imshow('tmp', fin.astype(np.uint8))
#         # cv2.waitKey(0)
#         # return
#
#         # print(np.amax(fin), np.amin(fin))
#         # fin = fin/(10*len(self.panid2fill))
#
#         fin = fin.astype(np.uint8)
#         fin[fin < 1] = 128
#         # fin = cv2.GaussianBlur(fin, (51, 51), 0)
#         dutils.merge_lab(rgb=os.path.join(self.original_path, folder + '.jpg'), lab=fin,
#                          dst_path=os.path.join(self.outdir, folder + '.jpg'))
