import json
import os
import dlib.dutils as dutils
import v3_0.src.lib.mycocotools as mcoto
import v3_0.src.utils_local as utl

# creator = mcoto.CocoPanopticDatasetCreator("../assets/ins3/sources/")
# creator.generate()
# mcoto.show_instance_contour("../assets/ins3/datasets/coco/annotations/instances_val2017.json", 884)
#
# dutils.amp_image(os.path.join('../assets/ins3/infero_pano/panoptic_seg/884/'+'ids.png'), 10, dst_path='../assets/others/yo.png')

# merger = mcoto.PanopticMerger("../assets/ins3/infero_pano/")
# # merger.GAUSSIAN_BLUR = 51
# merger.merge()
# mcoto.average_ab(os.path.join('../assets/ins3/infero_pano_merged/'))

# pant = [[884, ], ]
# ori = [[811, 1344], ]
#
# utl.make_batch(pant, ori)
# ep = 3000
ep = 100
# ep = 3400

for i in range(0, int(ep/1000)+1):
    print(i*1000 + ep%1000)
