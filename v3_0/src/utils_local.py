import json


def make_batch(panoptics, originals, outpath="../assets/full1/batch.json", epochs=5000):
    pngs = []
    tars = []
    for item in panoptics:
        start = item[0]
        end = item[-1]
        for i in range(start, end + 1):
            pngs.append(i)

    for item in originals:
        start = item[0]
        end = item[-1]
        for i in range(start, end + 1):
            tars.append(i)

    batch = {
        "pngs": pngs,
        "tars": tars,
        "epochs": epochs
    }

    json.dump(batch, open(outpath, 'w'), indent=4)
