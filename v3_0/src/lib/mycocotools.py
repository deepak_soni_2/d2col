import json, os, cv2, shutil
from copy import copy
import numpy as np
from shapely.geometry import Polygon, MultiPolygon
from PIL import Image
from tqdm import tqdm
import matplotlib.pyplot as plt
import dlib.dutils as dutils
from ast import literal_eval


class CocoPanopticDatasetCreator:
    def __init__(self, input_dir):
        self.MASK_AREA_THRESHOLD = 10
        self.indir = input_dir
        self.outdir = os.path.join(self.indir, '../datasets/')
        self.mask_dir = os.path.join(self.indir, 'panoptic')
        self.images_dir = os.path.join(self.indir, 'original')
        self.color_mapping = os.path.join(self.indir, 'color_mapping.json')
        self.img_out = os.path.join(self.outdir, "coco", "val2017")
        self.mask_out = os.path.join(self.outdir, "coco", "panoptic_val2017")
        self.gt_colors_list = None

    def create_sub_masks(self, mask_image):
        """
        This function takes a RGB image with color masks and create submask for each color
        :param mask_image: RGB image
        :return: array of submask
        """
        width, height = mask_image.size

        # Initialize a dictionary of sub-masks indexed by RGB colors
        sub_masks = {}
        foreign = Image.new('1', (width + 2, height + 2))
        for x in tqdm(range(width)):
            for y in range(height):
                # Get the RGB values of the pixel
                pixel = mask_image.getpixel((x, y))[:3]

                # If the pixel is not black...
                pixel_str = str(pixel)
                if pixel != (0, 0, 0) and pixel_str in self.gt_colors_list:
                    # Check to see if we've created a sub-mask...
                    sub_mask = sub_masks.get(pixel_str)
                    if sub_mask is None:
                        # Create a sub-mask (one bit per pixel) and add to the dictionary
                        # Note: we add 1 pixel of padding in each direction
                        # because the contours module doesn't handle cases
                        # where pixels bleed to the edge of the image
                        sub_masks[pixel_str] = Image.new('1', (width + 2, height + 2))

                    # Set the pixel value to 1 (default is 0), accounting for padding
                    sub_masks[pixel_str].putpixel((x + 1, y + 1), 1)
                elif pixel != (0, 0, 0):
                    foreign.putpixel((x + 1, y + 1), 1)
                    # print("foreign pixel found at : ", x, y)
        if np.sum(np.array(foreign)) > 0:
            print("total foreign pixels found : ", np.sum(np.array(foreign)))
            foreign.show()
            return None
        if self.MASK_AREA_THRESHOLD is not None:
            items = [item for item in sub_masks]
            for item in items:
                if np.sum(np.array(sub_masks[item])) < self.MASK_AREA_THRESHOLD:
                    sub_masks.pop(item).show()
                    print("Very small instance found ", item)
                    return None
        return sub_masks

    def create_sub_mask_annotation(self, sub_mask, image_id, category_id, annotation_id, is_crowd, color):
        cvimage = cv2.flip(cv2.rotate(cv2.UMat(np.array(sub_mask, dtype=np.uint8)), cv2.cv2.ROTATE_90_COUNTERCLOCKWISE), 0)
        regions, hierarchy = cv2.findContours(cvimage, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for i in reversed(range(len(hierarchy.get()[0]))):
            if hierarchy.get()[0][i][3] == 0:
                regions.pop(i)
        regions = [region.get()[:, 0, :] for region in regions]

        for i in reversed(range(len(regions))):
            if len(regions[i]) < 3:
                print("very small contour found : ", regions[i])

        segmentations = []
        polygons = []
        for contour in regions:
            # Flip from (row, col) representation to (x, y)
            # and subtract the padding pixel
            for i in range(len(contour)):
                row, col = contour[i]
                contour[i] = (col - 1, row - 1)

            # Make a polygon and simplify it
            poly = Polygon(contour)
            poly = poly.simplify(1.0, preserve_topology=False)
            polygons.append(poly)
            segmentation = np.flip(np.array(poly.exterior.coords), 0).ravel().tolist()
            segmentations.append(segmentation)
        # print(category_id, len(segmentations))

        # Combine the polygons to calculate the bounding box and area
        multi_poly = MultiPolygon(polygons)
        x, y, max_x, max_y = multi_poly.bounds
        width = max_x - x
        height = max_y - y
        x, width = np.clip((x, width), a_min=0, a_max=1920)
        y, height = np.clip((y, height), a_min=0, a_max=1080)
        bbox = (x, y, width, height)
        area = multi_poly.area

        annotation = {
            'segmentation': segmentations,
            'iscrowd': is_crowd,
            'image_id': int(image_id),
            'category_id': category_id,
            'id': annotation_id,
            'bbox': bbox,
            'area': area,
        }

        id_color = color.split('(')[1].split(',')
        R = int(id_color[0])
        G = int(id_color[1])
        B = int(id_color[2].split(')')[0])
        id_color = R + (G * 256) + (B * 256 * 256)
        seg_info = {
            "area": np.array(area, dtype=np.int).tolist(),
            "bbox": np.array(bbox, dtype=np.int).tolist(),
            "category_id": category_id,
            "id": id_color,
            "iscrowd": is_crowd
        }
        return annotation, seg_info

    def generate(self):
        dutils.create_dir_safely(self.outdir, overwrite='y')
        os.mkdir(os.path.join(self.outdir, "coco"))
        os.mkdir(os.path.join(self.outdir, "coco", "annotations"))
        os.mkdir(self.img_out)
        os.mkdir(self.mask_out)

        map_json = json.load(open(self.color_mapping))
        thing_id_list = map_json["coco_thing_id_list"]
        stuff_id_list = map_json["coco_stuff_id_list"]
        colid2isthing = {item["color_id"]:item["isthing"] for item in map_json["categories"]}
        colid2cocoid = {item: thing_id_list.pop(0) for item in list(colid2isthing) if colid2isthing[item] == 1}
        colid2cocoid.update({item: stuff_id_list.pop(0) for item in list(colid2isthing) if colid2isthing[item] == 0})
        gtcol2cocoid = {list(thing.keys())[0]: colid2cocoid[thing[list(thing.keys())[0]]] for thing in map_json["color_map"]}
        gtcol2isthing = {list(thing.keys())[0]: colid2isthing[thing[list(thing.keys())[0]]] for thing in map_json["color_map"]}
        self.gt_colors_list = [list(thing.keys())[0] for thing in map_json["color_map"]]
        ins_category_annotation = [{"id": colid2cocoid[item["color_id"]], "name": item["name"], "supercategory": item["name"]} for item in map_json["categories"] if item["isthing"] == 1]
        pan_category_annotation = [{"id": colid2cocoid[item["color_id"]], "isthing": item["isthing"], "name": item["name"], "supercategory": item["name"]} for item in map_json["categories"]]

        images = os.listdir(self.mask_dir)
        is_crowd = 0
        annotation_id = 1
        annotations = []
        pan_annos = []
        file_annos = []
        for image in images:
            image_id = image.split('.')[0]
            sub_masks = None
            while sub_masks is None:
                frame = Image.open(os.path.join(self.mask_dir, image))
                sub_masks = self.create_sub_masks(frame)
            width, height = frame.size
            seg_infos = []
            for color, sub_mask in sub_masks.items():
                category_id = gtcol2cocoid[color]
                annotation, seg_info = self.create_sub_mask_annotation(sub_mask, image_id, category_id, annotation_id, is_crowd, color)
                if gtcol2isthing[color] == 1:
                    annotations.append(annotation)
                    annotation_id += 1
                seg_infos.append(seg_info)
            pan_anno = {
                "file_name": image,
                "image_id": int(image_id),
                "segments_info": seg_infos
            }
            file_anno = {
                "file_name": image_id+'.jpg',
                "height": height,
                "id": int(image_id),
                "width": width
            }
            file_annos.append(file_anno)
            pan_annos.append(pan_anno)
            shutil.copyfile(os.path.join(self.mask_dir, image), os.path.join(self.mask_out, image))
            shutil.copyfile(os.path.join(self.images_dir, image.split('.')[0]+'.jpg'), os.path.join(self.img_out, image.split('.')[0]+'.jpg'))

        instance = {
            "annotations": annotations,
            "categories": ins_category_annotation,
            "images": file_annos
        }
        panoptic = {
            "annotations": pan_annos,
            "categories": pan_category_annotation,
            "images": file_annos
        }
        file = open(os.path.join(self.outdir, "coco", "annotations", "instances_val2017.json"), 'w')
        file.writelines(json.dumps(instance, indent=4, sort_keys=True))
        file.close()
        file = open(os.path.join(self.outdir, "coco", "annotations", "panoptic_val2017.json"), 'w')
        file.writelines(json.dumps(panoptic, indent=4, sort_keys=True))
        file.close()


def show_contours(arrs, dim):
    """
    shows contours plotted
    :param arrs: and array of coordinates in coco annotation format. arr((x1, y1, x2, y2, ....), (x1, y1, x2, y2, ....))
    :param dim: tuple (width, height) of output window
    """
    plt.axis([-20, dim[0]+20, -20, dim[1]+20])
    gca = plt.gca()
    gca.invert_yaxis()
    gca.set_aspect(1)
    gca.set_facecolor((0.0, 0.0, 0.0))
    for arr in arrs:
        allx = [arr[2 * i] for i in range(int(len(arr) / 2))]
        ally = [arr[2 * i + 1] for i in range(int(len(arr) / 2))]
        plt.plot(allx, ally)
    plt.show()


def show_instance_contour(instance_path, imageid):
    file = open(instance_path, 'r')
    data = json.load(file)
    pons = []
    [[pons.append(it) for it in item["segmentation"]] for item in data["annotations"] if item["image_id"] == imageid]
    for item in data["images"]:
        if item["id"] == imageid:
            show_contours(pons, (item["width"], item["height"]))
            return


class InstanceMerger:
    def __init__(self, indir, color_map_path):
        self.indir = indir
        self.outdir = os.path.join(indir, '../infero_instance_merged')
        self.color_mapping = color_map_path
        self.original_path = os.path.join(self.indir, "../sources/original/")
        self.insid2fill = None

    def create_id_maps(self):
        map_json = json.load(open(self.color_mapping))
        thing_id_list = map_json["coco_thing_id_list"]
        stuff_id_list = map_json["coco_stuff_id_list"]
        colid2fill = {item["color_id"]: item["fill_color"] for item in map_json["categories"]}
        colid2isthing = {item["color_id"]: item["isthing"] for item in map_json["categories"]}
        cocoid2colid = {thing_id_list.pop(0): item for item in list(colid2isthing) if colid2isthing[item] == 1}
        cocoid2colid.update({stuff_id_list.pop(0): item for item in list(colid2isthing) if colid2isthing[item] == 0})
        cocoid2fill = {item: colid2fill[cocoid2colid[item]] for item in cocoid2colid.keys()}
        insid2cocoid = {json.load(open(self.indir + 'ids.json'))[item]: item for item in json.load(open(self.indir + 'ids.json'))}
        self.insid2fill = {item: cocoid2fill[int(insid2cocoid[item])] for item in insid2cocoid}

    def simple_average_merger(self, folder):
        """
        creates an lab image with a and b are the averages of the a and b of the colors of the classes. these class imformation is in the name of the files in the given folder
        :param:folder: folder name in which all the pngs are present for each instance.
        :return:a lab image with ab average of all the png images according to instance.
        """
        files = os.listdir(os.path.join(self.indir, folder))
        dim = cv2.imread(os.path.join(self.indir, folder, files[0])).shape
        fin = np.zeros((dim[0], dim[1], 3), dtype=np.float)
        divisor = np.ones_like(fin) * 0.00000001
        for file in files:
            img = cv2.imread(os.path.join(self.indir, folder, file))
            divtmp = img
            cur = np.zeros(fin.shape, dtype=np.uint8)
            divisor = divisor + divtmp
            cur[img[:, :, 0] > 0] = dutils.rgb2cvlab(literal_eval(self.insid2fill[int(file.split('_')[1].split('.')[0])]))
            fin = fin + cur
        fin = np.divide(fin, divisor)
        fin = fin.astype(np.uint8)
        fin[fin < 1] = 128
        return fin

    def merge(self):
        dutils.create_dir_safely(self.outdir, overwrite='y')
        folders = os.listdir(self.indir)
        for folder in tqdm(folders):
            fin = self.simple_average_merger(folder)
            dutils.merge_lab(rgb=os.path.join(self.original_path, folder+'.jpg'), lab=fin, dst_path=os.path.join(self.outdir, folder+'.jpg'))


class PanopticMerger:
    def __init__(self, indir, outdir=None):
        self.indir = indir
        self.insdir = os.path.join(indir, 'instance/')
        self.pandir = os.path.join(indir, 'panoptic_seg/')
        if outdir is None:self.outdir = os.path.join(indir, '../infero_pano_merged')
        else: self.outdir = outdir
        self.color_mapping_path = os.path.join(indir, '../sources/color_mapping.json')
        self.original_path = os.path.join(self.indir, '../sources/original/')
        self.contid2fill = None
        self.GAUSSIAN_BLUR = None

    def create_id_maps(self):
        map_json = json.load(open(self.color_mapping_path))
        i = 0
        self.contid2fill = {}
        for item in map_json["categories"]:
            if item["isthing"] == 1:
                self.contid2fill[i] = item["fill_color"]
                i = i + 1
        for item in map_json["categories"]:
            if item["isthing"] == 0:
                self.contid2fill[i] = item["fill_color"]
                i = i + 1

    def merge(self):
        self.create_id_maps()
        dutils.create_dir_safely(self.outdir, overwrite='y')
        folders = os.listdir(self.pandir)
        for folder in tqdm(folders):
            fin = np.zeros((1080, 1920, 3), dtype=np.float)
            divisor = np.ones_like(fin) * 0.00000001
            info = json.load(open(os.path.join(self.pandir, folder, 'info.json')))
            pngid2contid = {item["id"]:item["category_id"] for item in info}
            pngid2fill = {item:self.contid2fill[pngid2contid[item]] for item in pngid2contid}
            img_ori = cv2.imread(os.path.join(self.pandir, folder, 'ids.png'))
            for item in pngid2fill:
                img = copy(img_ori)
                img[img != item] = 0
                img[img == item] = 1
                divtmp = img
                cur = np.zeros(fin.shape, dtype=np.uint8)
                divisor = divisor + divtmp
                cur[img[:, :, 0] == 1] = dutils.rgb2cvlab(literal_eval(pngid2fill[item]))
                fin = fin + cur
            fin = np.divide(fin, divisor)
            fin = fin.astype(np.uint8)
            fin[fin < 1] = 128
            if self.GAUSSIAN_BLUR is not None:
                fin = cv2.GaussianBlur(fin, (self.GAUSSIAN_BLUR, self.GAUSSIAN_BLUR), 0)
            dutils.merge_lab(rgb=os.path.join(self.original_path, folder + '.jpg'), lab=fin, dst_path=os.path.join(self.outdir, folder + '.jpg'))


def average_ab(indir, outdir=None):
    if outdir is None:
        outdir = os.path.join(indir, '../averaged/')
    dutils.create_dir_safely(outdir, overwrite='y')
    files = os.listdir(indir)
    for i in tqdm(range(2, len(files)-2)):
        imp2 = np.array(cv2.cvtColor(cv2.imread(indir + files[i - 2]), cv2.COLOR_RGB2LAB), dtype=np.float)
        imp = np.array(cv2.cvtColor(cv2.imread(indir + files[i-1]), cv2.COLOR_RGB2LAB), dtype=np.float)
        imc = np.array(cv2.cvtColor(cv2.imread(indir + files[i]), cv2.COLOR_RGB2LAB), dtype=np.float)
        imn = np.array(cv2.cvtColor(cv2.imread(indir + files[i+1]), cv2.COLOR_RGB2LAB), dtype=np.float)
        imn2 = np.array(cv2.cvtColor(cv2.imread(indir + files[i + 2]), cv2.COLOR_RGB2LAB), dtype=np.float)
        imc[:, :, 1:3] = (imp2[:, :, 1:3] + imp[:, :, 1:3] + imc[:, :, 1:3] + imn[:, :, 1:3] + imn2[:, :, 1:3])/5
        imc = imc.astype(np.uint8)
        imc = cv2.cvtColor(imc, cv2.COLOR_LAB2RGB)
        cv2.imwrite(outdir + files[i], imc)


# an array of visually distinct colors
color_rgb = [(140, 0, 19), (217, 137, 108), (76, 61, 0), (255, 166, 64), (51, 13, 33), (0, 64, 9), (0, 82, 204),
             (98, 54, 217), (32, 242, 0), (163, 0, 204), (96, 113, 128), (179, 134, 152), (153, 150, 115), (51, 39, 26),
             (178, 173, 89), (43, 38, 51), (89, 137, 179), (0, 41, 51), (102, 78, 51), (76, 0, 31), (182, 222, 242),
             (77, 77, 153), (61, 0, 77), (234, 242, 121), (77, 19, 65), (51, 0, 0), (57, 111, 115), (255, 0, 170),
             (115, 29, 40), (26, 36, 102), (48, 56, 64), (64, 81, 128), (230, 0, 214), (121, 96, 128), (178, 89, 101),
             (0, 10, 77), (64, 26, 0), (178, 80, 45), (128, 255, 178), (13, 33, 51), (172, 195, 230), (22, 89, 58),
             (229, 214, 0), (255, 115, 64), (229, 57, 126), (255, 128, 162), (153, 125, 115), (75, 115, 29),
             (115, 120, 153), (0, 155, 166), (48, 51, 13), (242, 198, 182), (153, 204, 167), (140, 56, 0), (0, 0, 191),
             (178, 116, 45), (125, 179, 89), (38, 13, 51), (197, 102, 204), (0, 94, 140), (37, 140, 0), (0, 31, 77),
             (226, 172, 230), (214, 242, 182), (217, 108, 166), (45, 51, 38), (0, 71, 77), (128, 77, 153), (62, 45, 89),
             (115, 61, 0), (178, 125, 89), (134, 176, 179), (67, 89, 85), (178, 98, 45), (163, 217, 206), (61, 77, 0),
             (82, 89, 67), (153, 38, 99), (86, 115, 86), (76, 50, 19), (166, 0, 66), (107, 115, 0), (54, 217, 76),
             (255, 64, 64), (178, 101, 89), (70, 140, 79), (217, 87, 0), (230, 218, 172), (77, 153, 138), (255, 0, 0),
             (54, 119, 217), (255, 213, 128), (115, 57, 96), (63, 29, 115), (26, 51, 46), (0, 51, 27), (102, 0, 255),
             (73, 67, 89), (0, 163, 204), (107, 0, 115), (115, 0, 61), (89, 67, 76), (102, 56, 26), (128, 96, 96),
             (186, 242, 121), (115, 31, 0), (51, 26, 26), (242, 0, 0), (242, 182, 190), (204, 51, 71), (242, 214, 182),
             (0, 217, 173), (115, 161, 230), (89, 45, 51), (22, 58, 89), (255, 140, 64), (255, 128, 128),
             (140, 105, 35), (173, 217, 0), (229, 176, 115), (140, 124, 105), (64, 255, 242), (115, 103, 57),
             (163, 163, 217), (115, 73, 57), (155, 166, 0), (217, 173, 0), (102, 87, 77), (0, 179, 95), (0, 226, 242),
             (64, 52, 48), (76, 19, 19), (255, 0, 102), (179, 45, 152), (129, 102, 204), (0, 92, 115), (37, 0, 140),
             (166, 22, 0)]
