import detectron2
from detectron2.utils.logger import setup_logger

setup_logger()
import numpy as np
import os, json, cv2, random, shutil
import d2col.dlib.dutils as dutils
from google.colab.patches import cv2_imshow
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from google.colab import files
from detectron2.data.datasets import register_coco_panoptic_separated
from detectron2.engine import DefaultTrainer
from tqdm import tqdm

cfg = get_cfg()
cfg.OUTPUT_DIR = 'drive/My Drive/Colab Notebooks/detectron/checkpoint_panorf'
cfg.merge_from_file(model_zoo.get_config_file("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml"))
cfg.DATASETS.TRAIN = ("phih_panoptic_separated",)
cfg.DATASETS.TEST = ()
cfg.DATALOADER.NUM_WORKERS = 2
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
    "COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml")  # Let training initialize from model zoo
# cfg.MODEL.WEIGHTS = ""
cfg.SOLVER.IMS_PER_BATCH = 2
cfg.SOLVER.BASE_LR = 0.00025  # pick a good LR
cfg.SOLVER.MAX_ITER = 10000  # 300 iterations seems good enough for this toy dataset; you will need to train longer for a practical dataset
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 512  # faster, and good enough for this toy dataset (default: 512)
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 12  # only has one class (ballon)
cfg.MODEL.PANOPTIC_FPN.INSTANCE_LOSS_WEIGHT = 1.0
cfg.MODEL.RETINANET.NUM_CLASSES = 12
cfg.MODEL.SEM_SEG_HEAD.NUM_CLASSES = 12
CUDA_LAUNCH_BLOCKING = 1


def prep_datasets(src):
    if os.path.exists("datasets_panoptic.zip"): os.remove("datasets_panoptic.zip")
    if os.path.exists("datasets"): shutil.rmtree("datasets")
    shutil.copyfile(os.path.join("/content/drive/My Drive/Colab Notebooks/detectron/", src),
                    os.path.join('/content/', src))
    dutils.unzip(src, '/content/')
    os.system('python detectron2/datasets/prepare_panoptic_fpn.py')
    register_coco_panoptic_separated("phih_panoptic", {}, "datasets/coco/val2017", "datasets/coco/panoptic_val2017",
                                     "datasets/coco/annotations/panoptic_val2017.json",
                                     "datasets/coco/panoptic_stuff_val2017/",
                                     "datasets/coco/annotations/instances_val2017.json")
    data = MetadataCatalog.get(cfg.DATASETS.TRAIN[0])
    data.stuff_classes = ['building', 'vegetation', 'sky']
    data.stuff_dataset_id_to_contiguous_id = {92: 1, 93: 2, 95: 3, 0: 0}
    print(MetadataCatalog.get(cfg.DATASETS.TRAIN[0]))


def train_loop(iter):
    cfg.SOLVER.MAX_ITER = iter
    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
    trainer = DefaultTrainer(cfg)
    trainer.resume_or_load(resume=True)
    trainer.train()


def batch_inference(src):
    if os.path.exists(src): os.remove(src)
    if os.path.exists("infer"): shutil.rmtree("infer")
    if os.path.exists("infero_pano"): shutil.rmtree("infero_pano")
    if os.path.exists('infero_pano.zip'): os.remove('infero_pano.zip')
    os.mkdir('infero_pano/')
    os.mkdir('infero_pano/instance/')
    os.mkdir('infero_pano/instance/pngs/')
    os.mkdir('infero_pano/sem_seg/')
    os.mkdir('infero_pano/sem_seg/pngs/')
    os.mkdir('infero_pano/panoptic_seg/')
    shutil.copyfile(os.path.join("/content/drive/My Drive/Colab Notebooks/detectron/", src),
                    os.path.join('/content/', src))
    dutils.unzip(src, '/content/')

    cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")  # path to the model we just trained
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.95  # set a custom testing threshold
    cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = 0.95
    cfg.MODEL.PANOPTIC_FPN.COMBINE.OVERLAP_THRESH = 0.95
    cfg.MODEL.PANOPTIC_FPN.COMBINE.STUFF_AREA_LIMIT = 10000

    predictor = DefaultPredictor(cfg)
    files = os.listdir('infer')
    for item in tqdm(files):
        im = cv2.imread('infer/' + item)
        outputs = predictor(im)

        meta = MetadataCatalog.get(cfg.DATASETS.TRAIN[0])
        data = {"thing_dataset_id_to_contiguous_id": meta.thing_dataset_id_to_contiguous_id,
                "thing_classes": meta.thing_classes,
                "stuff_dataset_id_to_contiguous_id": meta.stuff_dataset_id_to_contiguous_id,
                "stuff_classes": meta.stuff_classes}

        classes = outputs["instances"].get_fields()['pred_classes'].cpu().detach().numpy()
        masks = np.array(outputs["instances"].get_fields()['pred_masks'].cpu().detach().numpy(), dtype=np.uint8)
        os.mkdir('infero_pano/instance/' + item.split('.')[0])
        for i in range(len(classes)):
            cv2.imwrite('infero_pano/instance/' + item.split('.')[0] + '/' + str(i) + '_' + str(classes[i]) + '.png',
                        masks[i])
        json.dump(data, open('infero_pano/meta.json', 'w'), indent=4)

        sems = outputs['sem_seg'].cpu().detach().numpy()
        os.mkdir('infero_pano/sem_seg/pngs/' + item.split('.')[0])
        for idx, img in enumerate(sems):
            cv2.imwrite('infero_pano/sem_seg/pngs/' + item.split('.')[0] + '/' + str(idx) + '.png', img)

        os.mkdir('infero_pano/panoptic_seg/' + item.split('.')[0])
        cv2.imwrite('infero_pano/panoptic_seg/' + item.split('.')[0] + '/ids.png',
                    outputs['panoptic_seg'][0].cpu().detach().numpy())
        json.dump(outputs['panoptic_seg'][1],
                  open('infero_pano/panoptic_seg/' + item.split('.')[0] + '/info.json', 'w'), indent=4)

    shutil.make_archive('infero_pano', 'zip', 'infero_pano')